# ☕
# C vs. Java
Kurze, vergleichende Einführung.

<small><a href="https://florianknoch.gitlab.io/c-vs-java/">https://florianknoch.gitlab.io/c-vs-java</a></small>

---

## Startklar?
`$ java -version` <!-- .element class="highlighted" -->

Gibt die aktuell installierte Javaversion aus.

`$ javac -version` <!-- .element class="highlighted" -->

Gibt die Version des installierten Java-Compilers aus.

<small>Achtung: `$` muss nicht mit eingegeben werden.</small> 

---

Die Ergebnisse sollten in etwa so aussehen:
<!-- ToDo: Text für Version 11 ausgeben lassen und hier einbauen -->

```bash
$ java -version
openjdk version "1.8.0_191"
OpenJDK Runtime Environment (build 1.8.0_191-8u191-b12-2ubuntu0.18.04.1-b12)
OpenJDK 64-Bit Server VM (build 25.191-b12, mixed mode)

$ javac -version
javac 1.8.0_191
```

---

> C vs. Java

### Programmstruktur

```c
#include <stdio.h>
int main(void){
	// Anweisungen
	return 0;
}
```

```java
public class Test {
  public static void main(String[] args) {
    // Anweisungen
  }
}
```
<!-- .element class="fragment" -->

---


<!-- ## Java-Programme ausführen

`$ cd C:/Pfad/zum/Verzeichnis` <!~~ .element class="highlighted" ~~>

Navigiert zum angegebenen Verzeichnis.

`$ javac Klasse.java` <!~~ .element class="highlighted" ~~>

Kompiliert den Quelltext in `Klasse.java`.

`$ java Klasse` <!~~ .element class="highlighted" ~~>

Führt die kompilierte Klasse `Klasse.class` aus.

-->

> C vs. Java

### Ausgaben

```c
int a = 12;
printf("a = %d\n", a);
```

```java
int a = 12;
System.out.println("a = " + a);

// Möglich, aber seltener verwendet:
System.out.printf("a = %d\n", a); 
```
<!-- .element class="fragment" -->

---

# Aufgabe 1.1

```bash
# Befehle zum Kompilieren:
$ javac StaticIsForBeginners.java
$ java StaticIsForBeginners
```

---

> C vs. Java

### Arrays

```c
int lengthA = 12;
int* arrayA = malloc(lengthA * sizeof(int));
```

```java
int lengthA = 12;
int[] arrayA = new int[lengthA];
```
<!-- .element class="fragment" -->

```c
int arrayB[] = {1, 2, 3};
```
<!-- .element class="fragment" -->

```java
int[] arrayB = {1, 2, 3};
System.out.println(arrayB.length); // 3
```
<!-- .element class="fragment" -->

---

# Aufgaben 1.2/1.3

```bash
# Befehle zum Kompilieren:
$ javac StaticIsForBeginners.java
$ java StaticIsForBeginners
``` 

---

> C vs. Java

### Datenstrukturen

```c
#include <stdio.h>
struct CarStruct {
    char* modelName;
    int maxSpeed;
    int currentSpeed;
};
typedef struct CarStruct *Car;

Car createCar(...) { ... }
void accelerateCar(Car car, ...) { ... }
```

---

> C vs. Java

### Datenstrukturen

```java
public class Car {
  private String modelName;
  private int maxSpeed;
  private int currentSpeed;

  public Car(...) { ... }
  public void accelerate(...) { ... }
}
```

---

### Konstruktoren in Java

```java
public class Car {
  private String modelName;
  private int maxSpeed;
  private int currentSpeed;
  
  public Car() { 
    this.modelName = "DefaultModel";
    this.maxSpeed = 0;
    this.currentSpeed = 0;
  }
  
  public Car(String modelName, int maxSpeed) { 
    this.modelName = modelName;
    this.maxSpeed = maxSpeed;
    this.currentSpeed = 0;
  }
}
```
<!-- .element style="font-size: 1rem;" -->

---

> C vs. Java

### Datenstrukturen

```c
int main(void) {
  Car c1 = createCar(...);
  accelerateCar(c1, ...);
  return 0;
}
```

```java
public class CarTest {
  public static void main(String[] args) {
    Car c1 = new Car(...);
    c1.accelerate(...);
  }
}
```
<!-- .element class="fragment" -->

---

### Schlüsselwort `static`

<div style="width: 60vw; display: grid; grid-template-columns: 50% 50%; grid-gap: 4px;">
  <div>
  <pre style="margin: 0"><code class="hljs lang-java" style="font-size: 1rem;">public class Math {
  private double PI = 3.1415926535897932;
    
  public double getPi() {
    return this.PI;
  }
  
  public static void main(String[] args) {
    Math math = new Math();
    double diameter = 12.42;
    double area = math.getPi() · diameter;
  }
}</code></pre>
</div>
  <div class="fragment">
  <pre style="margin: 0"><code class="hljs lang-java" style="font-size: 1rem;">public class MathStatic {
  private static double PI = 3.1415926535897932;
    
  public static double getPi() {
    return this.PI;
  }
  
  public static void main(String[] args) {
    double diameter = 12.42;
    double area = Math.getPi() · diameter;
  }
}</code></pre>
</div>
</div>

---

> C vs. Java

### Sichtbarkeiten

| Zugriff erlaubt bei/aus | gleicher Klasse | gleichem Paket | Unterklasse außerhalb Paket | "Weltfremdem" Paket |
| --- | --- | --- | --- | --- |
| private      | ✅ | 🚫 | 🚫 | 🚫 |
| &lt;default> | ️️✅ | ✅ | 🚫 | 🚫 |
| protected    | ✅ | ✅ | ✅ | 🚫 |
| public       | ✅ | ✅ | ✅ | ✅ |
<!-- .element style="font-size: 80%;" -->

<small><b>Quelle:</b> Skript zu DSG-JaP-B von Prof. Dr. Guido Wirtz, Seite III-29, <br/>Otto-Friedrich-Universität Bamberg</small>

---


### Projektstruktur

<div style="display: grid; grid-template-columns: 50% 50%; grid-gap: 2px;">
  <div>
    <ul>
      <li>src-Ordner ist für Quelltext, bin-Ordner für kompilierte Dateien vorbehalten</li>
      <li>Packages werden abgebildet durch Ordner und Verweis in jeder Datei</li>
    </ul>
  </div>
  <div>
    <pre><code class="lang-md hljs markdown" style="height: 100%;">.
├── bin
│   └── car
│       └── Car.class
└── src
    └── car
        └── Car.java  </code></pre>
    <pre><code class="lang-java hljs markdown" style="height: 100%;">package car;

public class Car {
  // ...
}</code></pre>
  </div>

---

### JavaDoc

```java
/**
 * This method divides two integers.
 * @param a the dividend
 * @param b the divisor
 * @return the ratio between dividend and divisor
 * @throws IllegalArgumentException if b is zero
 */
public float divide(int a, int b) {
  // ...
}
```

```
$ javadoc -d ./docs path/to/java/files/*.java
```

---

### Hilfreiches

* ["Java Programming Cheatsheet" der Princeton University](https://introcs.cs.princeton.edu/java/11cheatsheet/)
* ["C Programming vs. Java Programming"](https://introcs.cs.princeton.edu/java/faq/c2java.html)
* ["C vs Java"](https://www.educba.com/c-vs-java/)
* ["10 Major Differences Between C And JAVA"](http://durofy.com/10-major-differences-between-c-and-java)

---

# Fragen?
