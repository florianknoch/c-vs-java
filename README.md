# ANSI C vs. Java
Diese auf [Reveal.js](https://revealjs.com/) aufbauende Präsentation gibt einen kurzen, vergleichenden Überblick über die wichtigsten Unterschiede zwischen ANSI C und Java 8. Es ist gedacht für Programmieranfänger, die bisher nur mit ANSI C gearbeitet haben. Viele Dinge können glücklicherweise nahezu unverändert in Java übernommen werden. Zum Selbststudium ist die Präsentation nur bedingt geeignet, hierfür seien folgende weiteren Materialien empfohlen:

* "Java Programming Cheatsheet" der Princeton University, https://introcs.cs.princeton.edu/java/11cheatsheet/
* "C Programming vs. Java Programming", https://introcs.cs.princeton.edu/java/faq/c2java.html
* "C vs Java", https://www.educba.com/c-vs-java/
* "10 Major Differences Between C And JAVA", http://durofy.com/10-major-differences-between-c-and-java